#include <iostream>
#include <string>
#include "Gods.h"
#include "Inventory.h"
#include "MagicBook.h"

string input;
MagicBook mb;
Gods gods;
Inventory inven;

void Check(string input);
int main()
{
	bool thorns = false, monster = false, statues = false, explored = false, checking = false;
	bool running = true;
	cout << "In a distant world, you have ventured into a deep dungeon. There was a cave-in, and you have been trapped inside.\n";
	cout << "You only have your book of magic, your bag, and the gods themselves. \n";
	cout << "You appear to be in a somewhat large area, ahead is a long hallway, to your left is a mysterious gleam.\n";
	cout << "What would you like to do? Your options are limited. But at anytime you may CHECK your BOOK of spells.\n";

	while (running == true)
	{

		cout << "[Go Straight] \n[Explore Room] \n[Go Left] \n";

		getline(cin, input);
		system("cls");
		Check(input);
		if (input == "go straight")
		{
			if (monster == false)
			{
				cout << "You forge onwards, and eventually come across a vicious monster!\n";
				cout << "It appears to be a terrible monster wreathed in fire. Perhaps a water spell may prove useful.\n";
				cout << "It hasn't seemed to notice you yet. What will you do? \n";
				cout << "[Fight the Monster] [Leave Room]\n";
				getline(cin, input);
				if (input == "fight monster" || input == "fight")
				{
					cout << "The monster lets out a mighty roar as you assume a combat stance.\n";
					cout << "What will you do now?\n";
					cout << "[Cast Spell] [Run!]\n";
					getline(cin, input);
					if (input == "run")
					{
						cout << "You turn tail and flee back into the hall you came from.\n";
					}
					else if (input == "cast" || input == "cast spell")
					{
						cout << "You cast the spell, " << mb.CastSpell() << ".\n";
						if (mb.CastSpell() == "Fireball")
						{
							mb.PopSpell();
							cout << "The monster absorbs your fireball with a gleeful scream!\n It turns on you for it's next meal!\n";
							cout << "You have died!\n";
							running = false;

						}
						else if (mb.CastSpell() == "Aqua Lance")
						{
							mb.PopSpell();
							monster = true;

							cout << "The monster shrieks with horror as it's pierced by your spell and the fires are extinguished!\n";
							cout << "Having defeated the monster, you move onwards.\n";
						}
						while (statues == false)
						{
							cout << "You come to a room where 6 statues guard a large door.\n At their feet is a large stone tablet with some writing on it, However you can't quite make it out.\n";

							cout << "What will you do? \n[Leave Room] [Approach Statues] [Examine Tablet]\n";
							getline(cin, input);

							if (input == "leave")
							{
								cout << "You return to the main area.\n";
								break;
							}
							if (input == "approach" || input == "approach statues" || input == "statues")
							{
								while (checking == false)
								{
									cout << "You stand before the statues, each of them seem to depict an image of a deity.\n"
										"Which statue will you approach first?\n"
										"[Back away] \n[Statue of Zeus] \n[Statue of Aries] \n[Statue of Jenova] \n[Statue of Helios] "
										"\n[Statue of Ragnarok] \n[Statue of Lunaris]\n";
									getline(cin, input);
									if (input == "back")
									{
										cout << "You return to the wide room.\n";
										checking = true;

									}
									if (input == "lunaris" || input == "statue lunaris")
									{
										cout << "You approach the statue of Lunaris, Goddess of the moons, gentle and serene.";
									}
								}
							}
							if (input == "examine" || input == "examine tablet")
							{
								cout << "You approach the tablet and read it's inscription.\n "
									"Ye who enter this cursed hallow. Know only the gods can save thy soul\n"
									"Give thanks to the moon er' the sun. Make way for change and war.\n "
									"Bow to the shepherd and his king.\n";

							}
						}
					}
				}
			}

			if (input == "leave" || input == "leave room")
			{
				cout << "You return to the previous area.\n";
			}
		}
		if (input == "explore" || input == "explore room")
		{
			if (explored == false) {
				explored = true;
				cout << "You explore your current room for anything that may help you on your journey.\n";
				cout << "You come across a small key! You stow it away in your inventory.\n";
				inven.AddItem("Small Key");
				cout << "You also come across a bottle of wine... It might be nice for your time here.\n";
				inven.AddItem("Wine");
			}
			else if (explored)
			{
				cout << "You already explored this area.\n";
			}
		}
		if (input == "go left" || input == "left")
		{
			cout << "You go into the room with the mysterious gleam, and you find a treasure chest!\n";
			if (thorns == false)
			{
				cout << "It's also guarded by thorny vines. Mayhap a fire spell would clear them out?\n";

				cout << "[Leave] [Cast Spell]\n";
				getline(cin, input);
				if (input == "cast" || input == "cast spell")
				{
					cout << "You cast " << mb.CastSpell() << ". \n";
					if (mb.CastSpell() == "Fireball")
					{
						thorns = true;
						mb.PopSpell();
						cout << "The fires of your magic burn the thorns to cinders, leaving some piping hot treasure.\n";
					}
				}
				if (input == "leave")
				{
					cout << "You return to the main area.\n";


				}

			}
			if (thorns == true)
			{
				cout << "The treasure chest appears locked, if only you had some sort of key.\n";
				cout << "What do you want to do? \n[Use Item] [Leave]\n";
				getline(cin, input);

				if (input == "leave")
				{
					cout << "You return to the main area.\n";
				}
				if (input == "use item" || input == "use");
				{
					if (inven.GetInventory() == 0)
					{
						cout << "You have nothing in your inventory!\n";
						//cout << "You return to the main room. \n";
					}
					else if (inven.GetItem() == "Small Key")
					{
						inven.UseItem();
						cout << "\nYou have found a magic orb in the chest!\n";

						inven.AddItem("Magic Orb");
						cout << "You return to the main room. \n";
					}
				}

			}
		}
	}
	system("pause");
	return 0;
}
void Check(string input)
{
	if (input == "check book" || input == "check")
	{
		mb.DisplaySpells();
		system("pause");
	}

}