#pragma once
#include <iostream>
#include <vector>
#include <queue>
using namespace std;
class MagicBook
{
private:
	vector<string> magicBook;
	queue<string> spells;

public:
	MagicBook();
	void DisplaySpells()
	{
		cout << "Your book was damaged in the fall, it looks like you can only cast the first spell in your book.\n";
		cout << "These are your available spells.\n";
		
		for (int i = 0; i < magicBook.size(); i++)
		{
			cout << magicBook[i] << " ";
			cout << endl;
		}
	}
	string CastSpell()
	{
		return spells.front();

	}
	void PopSpell()
	{
		spells.pop();
	}
};

