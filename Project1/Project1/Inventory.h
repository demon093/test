#pragma once
#include <iostream>
#include <vector>
#include <queue>
using namespace std;


class Inventory
{
private:
	queue<string> inventory;
public:
	void UseItem()
	{
		cout << "You have used " << inventory.front() << " ";
		inventory.pop();
	}
	int GetInventory()
	{
		return inventory.size();
	}
	string GetItem()
	{
		return inventory.front();
	}
	void AddItem(string item)
	{
		inventory.push(item);
	}
};

